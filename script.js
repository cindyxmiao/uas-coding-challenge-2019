/*
* Given an integer n, calculate the nth row of Pascal's triangle
* 
* @param n
*	 row number of Pascal's triangle, where the first row is n = 0
* 
* @return 
*	an array of the nth row of Pascal's triangle,
* 	or an empty array if n is not valid
*/

//some tests
console.log(ptRow(4).toString());
console.log(ptRow(7).toString());
console.log(ptRow(0).toString());
console.log(ptRow(-1).toString());


function ptRow(n)
{
    var row = [];
    if (n < 0){
        return row;
    }
 
    for (col = 0; col < n+1 ; col++){ 
        row [col] = getFactorial(n) / (getFactorial(col) * getFactorial((n-col)));
    }
    return (row);
}


function getFactorial(num){

    if (num == 0){
        return 1;
    } 

    factorial = num;
    for ( i = 1; i <num; i++){
        factorial *= (num - i);
    }
    return factorial;
}
